#/bin/bash

TOKEN=$1
PROJECT_ID=$2
PIPELINE_ID=$3

if [ $# -ne 4 ]; then
	echo "Syntax $0 <token> <projectId> <pipelineId>"
	exit 1
fi

if [ -x $POLL_INTERVAL ]; then
	POLL_INTERVAL=30
fi

if [ -x $BASE_URL ]; then
	BASE_URL=https://gitlab.com
fi

echo "Base URL: $BASE_URL"
echo "Poll interval: $POLL_INTERVAL"
echo "Project Id: $PROJECT_ID"
echo "Pipeline Id: $PIPELINE_ID"

function getStatus () {
	curl -s --header "PRIVATE-TOKEN:${TOKEN}" \
	 	"${BASE_URL}/api/v4/projects/${PROJECT_ID}/pipelines/${PIPELINE_ID}" | jq -r '.status'
}

function isPending () {
	case $1 in
		pending)
			return 0
			;;
		running)
			return 0
			;;
	esac

	return 1
}

STATUS=`getStatus $PIPELINE_ID`

while isPending $STATUS
do
	echo "Waiting ${POLL_INTERVAL} seconds after getting status = $STATUS"
	sleep $POLL_INTERVAL
	STATUS=`getStatus $PIPELINE_ID`
done

if [ "$STATUS" == "success" ]
then
	echo "Pipeline success"
	exit 0
else
	echo "Pipeline failed"
	exit 1
fi